<?php
function themeslug_enqueue() {
		wp_enqueue_style('reset_css', get_template_directory_uri()."/css/reset.css");
		wp_enqueue_style('bootstrap_css', get_template_directory_uri().'/css/bootstrap.min.css');
		wp_enqueue_style('animate_css', get_template_directory_uri().'/css/animate.min.css');
		wp_enqueue_style('fontawesome_css', get_template_directory_uri().'/css/font-awesome.min.css');
		wp_enqueue_style('owlcarousel_css', get_template_directory_uri().'/css/owl.carousel.css');
		wp_enqueue_style('socials_css', get_template_directory_uri().'/css/socials.css');
		wp_enqueue_style('YTPlayer_css',get_template_directory_uri().'/css/YTPlayer.css');
		wp_enqueue_style('magnific-popup_css',get_template_directory_uri().'/css/magnific-popup.css');
		wp_enqueue_style('prettyPhoto_css',get_template_directory_uri().'/css/prettyPhoto.css');
		wp_enqueue_style('revslider_css',get_template_directory_uri().'/css/revslider.css');
		wp_enqueue_style('setting_css',get_template_directory_uri().'/css/settings.css');
		wp_enqueue_style('settingie8_css',get_template_directory_uri().'/css/settings-ie8.css');
		wp_enqueue_style('orange.css',get_template_directory_uri().'/css/orange.css');
		wp_enqueue_style('font_css',get_template_directory_uri().'/css/font.css');
		wp_enqueue_style('dark_css',get_template_directory_uri().'/css/dark.css');
		wp_enqueue_style('responsive_css',get_template_directory_uri().'/css/responsive.css');
		wp_enqueue_style('custom_css',get_template_directory_uri().'/css/style.css');
		wp_enqueue_style('style_css', get_template_directory_uri().'/style.css');

}


add_action( 'wp_enqueue_scripts', 'themeslug_enqueue' );

function theme_js(){

		wp_enqueue_script('analytic_js',get_template_directory_uri().'/js/analytic.js');
		//wp_enqueue_script('jquery_js',get_template_directory_uri().'/js/jquery-1.11.0.min.js');
		
		wp_enqueue_script('bootstrap_js',get_template_directory_uri().'/js/bootstrap.min.js');
		wp_enqueue_script('easing_js',get_template_directory_uri().'/js/jquery.easing.1.3.js');
		wp_enqueue_script('appear_js',get_template_directory_uri().'/js/jquery.appear.js');
		wp_enqueue_script('prettyPhoto_js',get_template_directory_uri().'/js/jquery.prettyPhoto.js');
		wp_enqueue_script('modernizr-latest_js',get_template_directory_uri().'/js/modernizr-latest.js');
		wp_enqueue_script('smoothscroll_js',get_template_directory_uri().'/js/SmoothScroll.js');
		wp_enqueue_script('superslides_js',get_template_directory_uri().'/js/jquery.superslides.js');
		wp_enqueue_script('isotope_js',get_template_directory_uri().'/js/jquery.isotope.js');
		wp_enqueue_script('parallax_js',get_template_directory_uri().'/js/jquery.parallax-1.1.3.js');
		wp_enqueue_script('fitvids_js',get_template_directory_uri().'/js/jquery.fitvids.js');
		wp_enqueue_script('flexslider_js',get_template_directory_uri().'/js/jquery.flexslider.js');
		wp_enqueue_script('owl-carousel_js',get_template_directory_uri().'/js/owl.carousel.js');
		wp_enqueue_script('YTPlayer_js',get_template_directory_uri().'/js/jquery.mb.YTPlayer.js');
		wp_enqueue_script('magnific-popup_js',get_template_directory_uri().'/js/jquery.magnific-popup.min.js');
		wp_enqueue_script('sticky_js',get_template_directory_uri().'/js/jquery.sticky.js');	

		//wp_enqueue_script('revolution_js',get_template_directory_uri().'/js/revslider/jquery.themepunch.revolution.min.js');
		//wp_enqueue_script('plugins_js',get_template_directory_uri().'/js/revslider/jquery.themepunch.plugins.min.js');
		//wp_enqueue_script('revslider_js',get_template_directory_uri().'/js/revslider/revslider.js');

		wp_enqueue_script('plugin_js',get_template_directory_uri().'/js/plugins.js');
		//wp_enqueue_script('google-map_js',get_template_directory_uri().'/js/google-map.js');
}

add_action( 'wp_enqueue_scripts', 'theme_js' );?>
