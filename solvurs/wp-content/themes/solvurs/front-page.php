		
<?php get_header() ?>
		<div class="row">
			<div class="col-sm-6">
				<div id="full-slider-wrapper" class="container hidden-xs">
					<?php putRevSlider( "home-slider" ) ?>
				</div>
				<!-- End Slider Wrapper -->
			</div>
			<div class="col-sm-6">
				<!-- Page Content -->
				<section class="page-content">
					<!-- Inner -->
					<div class="page-content-inner clearfix">

						<!-- Content Left -->
						<div class="content-left white">
							<!-- Header -->
						  <h1 class="content-head oswald uppercase">
								We Create Differentiation 
						  </h1>
							<!-- Description -->
							<p class="content-text">At Solvurs we strive to be the most reliable and creative event management company in Pakistan, always delivering the high standard of quality and value for money to our customers. We work in close partnership with our customers to offer a service that sets new standards in event management, with our customers needs always at the forefront of our mind. </p>
							<p class="content-text">For Solvurs each event is a custom project, irrespective of size or budget. This is what makes us deferent.<br>
							In short Solvurs events are passionate dedicated and fun when it comes to staging events................<br>
							now lets us make the differentiation for you! </p>

						</div>
						<!-- End Content Left -->

						

					</div><!-- End Inner -->
				</section>
				<!-- End Page Content -->				
			</div>
		</div>
		<!-- About Section -->
		<section class="container">
			<!-- About Inner -->
			<div class="inner about">
				<!-- Header -->
				<h1 class="header uppercase dark oswald animated" data-animation="fadeIn" data-animation-delay="100">
					Why Solvurs?
				</h1>
				<!-- Header Strip(s) -->
				  <div class="header-strips-one animated" data-animation="fadeIn" data-animation-delay="100"></div>
					<!-- Header Description -->
			  	<p class="description normal animated" data-animation="fadeIn" data-animation-delay="100">“Retaining the personal touch of a family company, we distinguish ourselves through an acute attention  to detail in terms of the needs of our clients Our strength lies in understanding every aspect of your requirements and our focus is on meeting and surpassing your expectations in every respect.”</p>
			  	<br><br>
			</div> 

	  		<!-- End About Inner -->
		</section>
		<!-- About Section -->
		<!-- Featured Works -->
		<section id="featured-works" class="container  parallax4">
			<!-- Inner -->
			<div class="inner fullwidth">
				<!-- Header -->
				<h1 class="header uppercase white oswald">
					our services
				</h1>
				<!-- Header Strip(s) -->
				<div class="header-strips-two"></div>
				<!-- Header Description -->
				<!-- <h2 class="description white uppercase">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui.
				</h2> -->

				<!-- Works -->
				<div class="works white">

					<!-- Item -->
					<div class="item">
						<!-- Image, Buttons -->
						<div class="f-image" style="width:400px">
							<!-- Image -->
							<img src="<?php echo esc_url( get_template_directory_uri()); ?>/images/f1.jpg" alt="event services bg"/>

							<!-- Hover Tags, Link -->
							
							<!-- Detail -->
							<div class="f-button second">
								<a href="#" class="featured-ball second ex-link" >
									<i class="fa fa-plus"></i>							</a>						</div>
							<!-- End Detail -->
						</div>
						<!-- End Image, Buttons -->

						<!-- Texts -->
						<div class="texts">
							<!-- Item Header -->
							<h1 class="f-head oswald normal uppercase">
								Creative & Innovative Concepts</h1>

							<!-- Item Description -->
							<!-- <h2 class="f-text open-sans normal">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis.</h2> -->
						</div>
						<!-- End Texts -->
					</div>
					<!-- End Item -->


					<!-- Item -->
					<div class="item">
						<!-- Image, Buttons -->
						<div class="f-image" style="width:400px">
							<!-- Image -->
							<img src="<?php echo esc_url( get_template_directory_uri()); ?>/images/f2.jpg" alt="wedding services bg"/>

							<!-- Hover Tags, Link -->
							
							<!-- Detail -->
							<div class="f-button second">
								<a href="#" class="featured-ball second ex-link" >
									<i class="fa fa-plus"></i>							</a>						</div>
							<!-- End Detail -->
						</div>
						<!-- End Image, Buttons -->

						<!-- Texts -->
						<div class="texts">
							<!-- Item Header -->
							<h1 class="f-head oswald normal uppercase">
								Detailed Itinerary Planning	</h1>

							<!-- Item Description -->
							<!-- <h2 class="f-text content-text normal">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor.						</h2> -->
						</div>
						<!-- End Texts -->
					</div>
					<!-- End Item -->


					<!-- Item -->
					<div class="item">
						<!-- Image, Buttons -->
						<div class="f-image" style="width:400px">
							<!-- Image -->
							<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/f3.jpg" alt="corporate services bg"/>

							<!-- Hover Tags, Link -->
							
							<!-- Detail -->
							<div class="f-button second">
								<a href="#" class="featured-ball second ex-link" >
									<i class="fa fa-plus"></i>							</a>						</div>
							<!-- End Detail -->
						</div>
						<!-- End Image, Buttons -->

						<!-- Texts -->
						<div class="texts">
							<!-- Item Header -->
							<h1 class="f-head oswald normal uppercase">
								Accommodation Arrangements</h1>

							<!-- Item Description -->
							<!-- <h2 class="f-text open-sans normal">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris						</h2> -->
						</div>
						<!-- End Texts -->
					</div>
					<!-- End Item -->

				</div>
				<!-- End Works -->
	        	<div class="tp-caption customin customout slide-button uppercase" style="z-index: 2; margin-left: 120px;">
					<a href="/services" class="scroll">
						Read More
					</a>
				</div>
			</div><!-- End Inner -->
		</section>
		<!-- End featured Works -->   
	    <!-- Menu Section -->
		<section class="container">
			<!-- About Inner -->
			<div class="inner about">
				<!-- Header -->
				<h1 class="header uppercase dark oswald animated" data-animation="fadeIn" data-animation-delay="100">
					The Vision
				</h1>
				<!-- Header Strip(s) -->
			  	<div class="header-strips-one animated" data-animation="fadeIn" data-animation-delay="100"></div>
				<!-- Header Description -->
			  	<p class="description normal animated" data-animation="fadeIn" data-animation-delay="100">“To be the most Preferred Solution Provider in the industry of Event Management, Food & Beverages and Electronic Media”
	          	</p>
			</div>
			<!-- End Menu Inner -->
		</section>
		<!-- Menu Section -->
	    <!-- Services -->
		<section id="services" class="container">

			<!-- Inner -->
			<div class="inner">
				<!-- Header -->
				<h1 class="header uppercase white oswald">
					Mission
				</h1>
				<!-- Header Strip(s) -->
				<div class="header-strips-one"></div>
				<!-- Header Description -->
				<div class="description white">
					<p class="description normal animated fadeIn visible">Our aim is to make everyone’s business profitable by providing expert management services and value-driven solutions at competitive & affordable rates
					</p><br>
			  </div>
			</div><!-- End Inner -->
		</section>
		<!-- End Skills -->
	    <!-- career -->
		<section class="container">
			<!-- career Inner -->
			<div class="inner about">
				<!-- Header -->
				<h1 class="header uppercase dark oswald animated" data-animation="fadeIn" data-animation-delay="100">
					Our Values
			  	</h1>
				<!-- Header Strip(s) -->
			  	<div class="header-strips-one animated" data-animation="fadeIn" data-animation-delay="100">
				</div>
				
				<p class="description normal animated fadeIn visible">Our company values are the yardstick for our thinking and actions. They are the essence of what ties us together today and tomorrow. 
				At SOLVURS HEALTH CARE, we do business on the basis of common values. Our success is based on courage, achievement, responsibility, respect, integrity, and transparency. These values determine our actions in our daily dealing with customers and business partners as well as in our teamwork and our collaboration with each other.&quot;</p>	

				<ul data-animation="fadeIn" data-animation-delay="100">
					<li class="description normal animated" data-animation="fadeIn" data-animation-delay="100">
						Integrity
					</li>
					<li class="description normal animated" data-animation="fadeIn" data-animation-delay="100">
						Commitment
					</li>
					<li class="description normal animated" data-animation="fadeIn" data-animation-delay="100">
						<h2 class="description normal animated" data-animation="fadeIn" data-animation-delay="100">Teamwork
					</li>
					<li class="description normal animated" data-animation="fadeIn" data-animation-delay="100">
						Professionalism
					</li>
					<li class="description normal animated" data-animation="fadeIn" data-animation-delay="100">
						Excellence and Dedication
					</li>
				</ul>
			</div> 

	  		<!-- End career Inner -->
		</section>
		<!-- About Section -->
		<!-- Testimonials -->
		<section id="testimonials" class="testimonials bg1 parallax3 t-center">

				<!-- Arrow -->
				<a class="t-arrow"></a>

				<!-- Quote -->
				<div class="quote white">
					<i class="fa fa-quote-right"></i>
				</div>
		        <!-- Header -->
					<h1 class="header uppercase white oswald">
						Who We Are?
					</h1>
					<!-- Header Strip(s) -->
					<div class="header-strips-two"></div>

					<!-- Text Slider -->
					<ul class="text-slider clearfix">

						<!-- Slide -->
						<li class="text normal">
							<!-- Quote -->
							<h1 class="white">
								Solvurs operations commenced in 2005 with humble beginning & since then there has been no looking back.
								Solvurs is a team of highly professional and enthusiastic experts, experienced in the fields of event management, corporate catering, food & beverages, local and international congresses
							</h1>
						</li>
						<!-- End Slide -->
					</ul><!-- End Text Slider -->
		</section>
		<!-- End Testimonials -->
		<!--contact -->
		<div class="container" id="email">
			<div class="inner">
		
				<div class="col-lg-12">
		
					<div class="col-lg-6" >
						<div class="panel panel-default" style="border:none; background-color:#4072AB; margin-top:20px;">

							<div class="panel-body img-rounded" style="background-color:#4072AB;">
								<form method="POST" action="thankyou.php" class="form-horizontal">
									<div class="form-group">
										<div class="col-lg-12">
											<input type="text" class="form-control" id="user-name" placeholder="Entre Your Name" required  name="name" />
										</div>
									</div><!--end from group-->
									<div class="form-group">
										<div class="col-lg-12">
											<input type="text" class="form-control" id="user-email" placeholder="Entre Your Email" required  name="email" />
										</div>
									</div><!--end from group-->
									<div class="form-group">
										<div class="col-lg-12">
											<input type="text" class="form-control" id="user-phone" placeholder="Entre Your Phone" required name="phone" />
										</div>
									</div><!--end from group-->
									<div class="form-group">
										<div class="col-lg-12">
											<textarea name="message" id="user-massage" class="form-control" cols="20" rows="5" placeholder="Entre Your Massege" required id="contactmessage"/></textarea>
										</div>
									</div><!--end from group-->
									<div class="form-group">
										<div class="col-lg-12">
											<button type="submit" class="btn btn-success" name="submit" id="send">Submit</button>
										</div>
									</div>
								</form>


							</div>
						</div>
		  			</div>
		
		
					<div class="col-lg-6">
						<!-- Google Map -->
						<section id="map">
							<!-- Google Map Script -->
							<!--<script async type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>-->				<?php echo do_shortcode("[huge_it_maps id='1']"); ?>
							<!-- Google Map ID -->
							<!--<div id="google-map"></div>-->
						</section>
						<!-- End Google Map -->
					</div>
		  		</div>
		  	</div>
		</div> 
		<!-- End Contact -->
		<!-- Adress Section -->
		<section id="address" class="container parallax7">
		  <div class="inner">
		    <!-- Address Soft Area -->
		    <div class="address-soft t-center">
		      <!-- Phone -->
		      <h2 class="phone-text oswald black"><a href="tel:+"> 03028540729 </a>, <a href="tel:+">03354346709</a></h2>
		      <!-- Address -->
		      <h2 class="phone-text oswald uppercase">Head Office: 71- D Block, Model Town, Lahore</h2>
		      <!-- E-Mail -->
		      <a href="#" class="mail-text uppercase oswald"> muzammil.mh@live.com, muzammil003@yahoo.com, </a>
		      <!-- Social, Facebook -->
		      <a href="#" target="_blank" class="social round dark-bg facebook"> <i class="fa fa-facebook"></i> </a>
		      <!-- Twitter -->
		      <a href="#" target="_blank" class="social round dark-bg twitter"> <i class="fa fa-twitter"></i> </a>
		      <!-- Instagram -->
		      <a href="#" target="_blank" class="social round dark-bg instagram"> <i class="fa fa-instagram"></i> </a>
		      <!-- YouTube -->
		      <a href="#" target="_blank" class="social round dark-bg youtube"> <i class="fa fa-youtube"></i> </a>
		      <!-- google plus -->
		      <a href="#" target="_blank" class="social round dark-bg google-plus"> <i class="fa fa-google-plus"></i> </a> </div>
		    <!-- End Address Soft Area -->
	      </div>
			
		  <!-- Inner --><!-- End Inner -->
		</section>
		<!-- End Address Section -->
<?php get_footer(); ?>		
