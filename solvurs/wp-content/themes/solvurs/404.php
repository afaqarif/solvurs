<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @package WordPress
 * @subpackage Greenwaycontrl
 * @since Green Way Control 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<header class="page-header">
				<h1 class="page-title"><?php echo 'Not Found'; ?></h1>
			</header>

			<div class="page-wrapper">
				<div class="page-content">
					<h2><?php echo 'This is somewhat embarrassing, isn’t it?'; ?></h2>
					<p><?php echo 'It looks like nothing was found at this location. Maybe try a search?'; ?></p>

					<?php  ?>
				</div><!-- .page-content -->
			</div><!-- .page-wrapper -->

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>