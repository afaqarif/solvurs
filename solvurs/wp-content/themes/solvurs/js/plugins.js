jQuery(function($) {
	$(window).load(function(){
	"use strict";
	$(".mid").delay(0).fadeOut(),
	$(".outter").delay(0).fadeOut(),
	$("#pageloader").delay(300).fadeOut("slow"),
	$(".fit-vids").fitVids(),
	$(".nav-toggle").hover(
		function(){
			$(this).find(".dropdown-menu").first().stop(!0,!0).fadeIn(250)
		},
		function(){
			$(this).find(".dropdown-menu").first().stop(!0,!0).fadeOut(250)
		}
	),
	$("#fullscreen , .p-section").superslides({play:12e3,animation:"fade",inherit_height_from:window}),
	$("#project").superslides({play:8e3,animation:"fade",inherit_width_from:window,inherit_height_from:".home_project"}),
	$(".v1 .text-slider").flexslider({
		animation:"slide",
		selector:"ul.home-texts li.slide",
		controlNav:!1,
		directionNav:!0,
		touch:!0,
		slideshowSpeed:5e3,
		direction:"vertical",
		start:function(){
			$("body").removeClass("loading")
		}
	}),
	$(".v2 .text-slider").flexslider({
		animation:"fade",
		selector:"ul.home-texts li.slide",
		controlNav:!1,
		directionNav:!0,
		animationSpeed:500,
		slideshowSpeed:5e3,
		direction:"vertical",
		start:function(){
			$("body").removeClass("loading")
		}
	}),
	$(".mp-image").magnificPopup({type:"image"}),
	$(".mp-video, mp-map").magnificPopup({type:"iframe"}),
	$.extend(!0,$.magnificPopup.defaults,{
		iframe:{
			patterns:{
				youtube:{
					index:"youtube.com/",
					id:"v=",
					src:"http://www.youtube.com/embed/%id%?autoplay=1"
				},
				vimeo:{
					index:"vimeo.com/",
					id:"/",
					src:"http://player.vimeo.com/video/%id%?autoplay=1"
				},
				gmaps:{
					index:"//maps.google.",
					src:"%id%&output=embed"
				}
			}
		}
	}),
	$(".mp-gallery").each(function(){
		$(this).magnificPopup({
			delegate:"a",
			type:"image",
			gallery:{enabled:!0}
		})
	}),
	jQuery("a[data-rel^='prettyPhoto']").prettyPhoto({
		theme:"light_square",
		default_width:700,
		default_height:400
	}),
	$(".fitvid").fitVids(),
	$(".custom_slider").flexslider({
		animation:"fade",
		selector:".image_slider .slide",
		controlNav:!0,
		directionNav:!0,
		animationSpeed:500,
		slideshowSpeed:5e3,
		pauseOnHover:!0,
		direction:"vertical",
		start:function(){
			$("body").removeClass("loading")
		}
	}),
	$(".inner-portfolio .text-slider").flexslider({
		animation:"fade",
		selector:"ul.texts li.slide",
		controlNav:!1,
		directionNav:!0,
		slideshowSpeed:5e3,
		direction:"vertical",
		start:function(){
			$("body").removeClass("loading")
		}
	}),
	$(".inner-portfolio .circle-image-slider").flexslider({
		animation:"fade",
		selector:"ul.circle-slider li.slide",
		controlNav:!1,
		directionNav:!0,
		slideshowSpeed:5e3,
		direction:"vertical",
		start:function(){
			$("body").removeClass("loading")
		}
	}),
	$(".home-boxes").owlCarousel({
		items:3,
		itemsDesktop:[1169,3],
		itemsDesktopSmall:[979,2],
		itemsTablet:[600,1],
		itemsTabletSmall:!1,
		itemsMobile:[560,1],
		slideSpeed:400
	}),
	$(".team-boxes").owlCarousel({
		items:4,
		itemsDesktop:[1169,4],
		itemsDesktopSmall:[1024,3],
		itemsTablet:[640,2],
		itemsTabletSmall:!1,
		itemsMobile:[560,1],
		slideSpeed:400}),
	$(".clients").owlCarousel({
		items:3,
		itemsDesktop:[1169,3],
		itemsDesktopSmall:[979,3],
		itemsTablet:[768,2],
		itemsTabletSmall:!1,
		itemsMobile:[479,1],
		mouseDrag:!1,
		pagination:!0,
		navigation:!1,
		touchDrag:!0,
		slideSpeed:400
	}),
	$(".logos").owlCarousel({
		items:5,
		itemsDesktop:[1169,5],
		itemsDesktopSmall:[979,4],
		itemsTablet:[768,3],
		itemsTabletSmall:!1,
		itemsMobile:[479,2],
		autoPlay:8e3,
		slideSpeed:400
	}),
	$("#featured-works .works").owlCarousel({
		items:4,
		itemsDesktop:[1169,4],
		itemsDesktopSmall:[1100,3],
		itemsTablet:[960,2],
		itemsMobile:[640,1],
		pagination:!1,
		navigation:!0,
		mouseDrag:!1,
		stopOnHover:!0,
		slideSpeed:700,
		paginationSpeed:900,
		rewindSpeed:1100
	}),
	$("#services .service-boxes").owlCarousel({
		items:4,
		itemsDesktop:[1169,4],
		itemsDesktopSmall:[979,3],
		itemsTablet:[768,2],
		itemsTabletSmall:!1,
		itemsMobile:[479,1],
		pagination:!1,
		navigation:!0,
		mouseDrag:!0
	}),
	$(".player").mb_YTPlayer(),
	$(".testimonials").flexslider({
		animation:"fade",
		selector:"ul.text-slider li.text",
		controlNav:!1,
		directionNav:!0,
		slideshowSpeed:5e3,
		direction:"vertical",
		start:function(){
			$("body").removeClass("loading")
		}
	}),
	$(".scroll").bind("click", function(e){
		var a=$(this), 
		t=$("#navigation,#navigation-sticky").outerHeight();
		$("html,body").stop().animate({
			scrollTop:$(a.attr("href")).offset().top-t+"px"},1200,"easeInOutExpo"),
			e.preventDefault()
		});
		var e=$("#navigation");
		// $(window).scroll(function(){
		// 	var a=$(this).scrollTop(),
		// 	t=$("#home").outerHeight(),
		// 	i=$("#navigation").outerHeight(),
		// 	o=$("#home").offset().top+t-i;a>=o?e.removeClass("first-nav").addClass("second-nav"):e.removeClass("second-nav").addClass("first-nav")
		// }),
		$("#back-top").hide(),
		$(window).scroll(function(){
			$(this).scrollTop()>500?$("#back-top").fadeIn():$("#back-top").fadeOut()
		}),
		$("form#contact-form").submit(function(){
			var e=/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/, 
			a=document.getElementById("email");
			if(e.test(a.value)?$(".email-missing").fadeOut("slow"):$(".email-missing").fadeIn("slow"),
				""==document.cform.name.value?$(".name-missing").fadeIn("slow"):$(".name-missing").fadeOut("slow"),
				""==document.cform.subject.value?$(".subject-missing").fadeIn("slow"):$(".subject-missing").fadeOut("slow"),
				""==document.cform.message.value?$(".message-missing").fadeIn("slow"):$(".message-missing").fadeOut("slow"),
				""==document.cform.name.value||!e.test(a.value)||""==document.cform.message.value
			)return!1;4
			if(""!=document.cform.name.value&&e.test(a.value)&&""!=document.cform.message.value){
				var t=$(this).serialize();
				$.post($(this).attr("action"),t,
				function(){
					$(".mail-message").removeClass("not-visible-message").addClass("visible-message")
				}
			)
		}return!1
	});
	var a=$(".w-items");a.isotope({
			resizable:!1,
			itemSelector:".w-item",
		hiddenStyle:{opacity:0},
		visibleStyle:{opacity:1},
		transformsEnabled:!1});var t=$("#w-options .w-option-set"),
		i=t.find("a");a.isotope({filter:".movie"}),
		i.click(function(){var e=$(this);if(e.hasClass("selected"))return!1;var t=e.parents(".w-option-set");t.find(".selected").removeClass("selected"),
			e.addClass("selected");var i={},
		o=t.attr("data-option-key"),
		l=e.attr("data-option-value");return l="false"===l?!1:l,
		i[o]=l,
		"layoutMode"===o&&"function"==typeof changeLayoutMode?changeLayoutMode(e,
			i):a.isotope(i),
		!1});var o=$(".portfolio-items");o.isotope({itemSelector:".item"});var t=$("#options .option-set"),
		i=t.find("a");i.click(function(){var e=$(this);if(e.hasClass("selected"))return!1;var a=e.parents(".option-set");a.find(".selected").removeClass("selected"),
			e.addClass("selected");var t={},
		i=a.attr("data-option-key"),
		l=e.attr("data-option-value");return l="false"===l?!1:l,
		t[i]=l,
		"layoutMode"===i&&"function"==typeof changeLayoutMode?changeLayoutMode(e,
			t):o.isotope(t),
		!1}),
		$("body").scrollspy({target:".nav-menu",
			offset:95})}),
	function(e){"use strict";e(document).ready(function(){function i(){a=t.any(),
			null==a&&(e("body.parallax .image1").parallax("50%",
			.5),
		e("body.parallax .image2").parallax("50%",
			.5),
		e("body.parallax .image3").parallax("50%",
			.5),
		e("body.parallax .image4").parallax("50%",
			.5),
		e("body.parallax .image5").parallax("50%",
			.5),
		e("body.parallax .image6").parallax("50%",
			.5),e("body.parallax .image7").parallax("100%",.5),e("body.parallax .image8").parallax("100%",.5),e("body.parallax .image9").parallax("100%",.5),e("body.parallax .image10").parallax("100%",.5),e("body.parallax .image11").parallax("100%",.5),e("body.parallax .image-bg").parallax("50%",.5),e("body.parallax .parallax").parallax("-50%",.3),e("body.parallax .parallax1").parallax("50%",.5),e("body.parallax .parallax2").parallax("50%",.5),e("body.parallax .parallax3").parallax("50%",.5),e("body.parallax .parallax4").parallax("50%",.5),e("body.parallax .parallax5").parallax("50%",.5),e("body.parallax .parallax6").parallax("50%",.5),e("body.parallax .parallax7").parallax("50%",.5))}e(window).bind("load",function(){i()}),i()});var a,t={Android:function(){return navigator.userAgent.match(/Android/i)},BlackBerry:function(){return navigator.userAgent.match(/BlackBerry/i)},iOS:function(){return navigator.userAgent.match(/iPhone|iPad|iPod/i)},Opera:function(){return navigator.userAgent.match(/Opera Mini/i)},Windows:function(){return navigator.userAgent.match(/IEMobile/i)},any:function(){return t.Android()||t.BlackBerry()||t.iOS()||t.Opera()||t.Windows()}}}(jQuery),jQuery(function(){$(".fact").appear(function(){$(".fact").each(function(){dataperc=$(this).attr("data-perc"),$(this).find(".factor").delay(6e3).countTo({from:0,to:dataperc,speed:3e3,refreshInterval:50})})})}),function(e){e.fn.countTo=function(a){a=e.extend({},e.fn.countTo.defaults,a||{});var t=Math.ceil(a.speed/a.refreshInterval),i=(a.to-a.from)/t;return e(this).each(function(){function o(){s+=i,n++,e(l).html(s.toFixed(a.decimals)),"function"==typeof a.onUpdate&&a.onUpdate.call(l,s),n>=t&&(clearInterval(r),s=a.to,"function"==typeof a.onComplete&&a.onComplete.call(l,s))}var l=this,n=0,s=a.from,r=setInterval(o,a.refreshInterval)})},e.fn.countTo.defaults={from:0,to:100,speed:1e3,refreshInterval:100,decimals:0,onUpdate:null,onComplete:null}}(jQuery),jQuery(".progress-bar").appear(function(){datavl=$(this).attr("data-value"),$(this).animate({width:datavl+"%"},"300")}),$(window).load(function(){"use strict";/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)?($(".mbYTP_wrapper").addClass("mobile-bg"),$("section , div").addClass("b-scroll"),$(".animated").addClass("visible")):(jQuery.browser.mozilla||jQuery.browser.safari||$("a.ex-link").click(function(){var e=this.getAttribute("href");return $("#pageloader").fadeIn(350,function(){document.location.href=e}),!1}),$(".animated").appear(function(){var e=$(this),a=e.data("animation");if(!e.hasClass("visible")){var t=e.data("animation-delay");t?setTimeout(function(){e.addClass(a+" visible")},t):e.addClass(a+" visible")}}))}),$(".mobile-nav-button").click(function(){$(".nav-inner div.nav-menu").slideToggle("medium",function(){})}),$(".nav-inner div.nav-menu ul.nav li a").click(function(){$(window).width()<1e3&&$(".nav-menu").slideToggle("2000")});
});