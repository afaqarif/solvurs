		
<?php get_header() ?>
		<section id="page" class="container">
				<?php 
					if(have_posts()): ?>
						<div class="page_header">
							<div class="inner">
								<h1 class="header uppercase oswald animated fadeIn visible" data-animation="fadeIn" data-animation-delay="100"><?php the_title(); ?></h1>
								<div class="header-strips-one animated fadeIn visible" data-animation="fadeIn" data-animation-delay="100"></div>
							</div>
						</div>
						<?php	while(have_posts()): the_post(); ?>
							<div class="page-content">
									<?php the_content(); ?>
							</div>
						<?php endwhile;
					endif;
				?>
		</section>
<?php get_footer(); ?>
		