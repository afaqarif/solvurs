<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
	    <meta charset="<?php bloginfo( 'charset' ); ?>" />
	    <title>Solvurs</title>
		<meta name="keywords" content="solvurs, event management services, wedding planner, party planner, event organizer Lahore"/>
	    <meta name="description" content="solvurs" />
	    <meta name="viewport" content="width=device-width" />
		<style type="text/css">

			body {
				margin-left: 0px;
			}
	    </style>
		<!-- End Theme Panel -->
		<!-- End CSS Files -->
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?> class="parallax">
		<!-- Page Loader--> 
		<section id="pageloader" class="white-bg">
			<div class="outter dark-border">
				<div class="mid dark-border"></div>
			</div>
		</section>
		<!-- Navigation -->
		<nav id="navigation-sticky" class="white-nav b-shadow">
			<!-- Navigation Inner -->
			<div class="nav-inner">
				<div class="logo">
					<!-- Navigation Logo Link -->
					<a href="/home">
						<!-- Your Logo -->
						<?php if(is_page('health-care')){ ?>

								<img src="/wp-content/uploads/2017/02/health-logo.jpg" class="site_logo" alt="Solvurs"/>
							<?php }else{ ?>
								<img src="/wp-content/uploads/2017/02/Picture2.jpg" class="site_logo" alt="Solvurs"/>
							<?php	} 
						?>
						
					</a>
				</div>
				<!-- Mobile Menu Button -->
				<a class="mobile-nav-button colored"><i class="fa fa-bars"></i></a>
				<!-- Navigation Menu -->
				<div class="nav-menu clearfix semibold">
					<ul class="nav uppercase oswald">
						<li><a href="home">Home</a></li>
	                    <li><a href="/about-us">About us</a></li>
						<li><a href="/event">Events</a></li>			
						<li><a href="/service" class="scroll">Services</a></li>
						<li><a href="/health-care" class="scroll">Health Care</a></li>
	                    <li><a href="/home/#address" class="scroll">Contact</a></li>
	                   	<li><a href="/home/#testimonials" class="scroll">Who's We Are</a></li>
					</ul>
				</div><!-- End Navigation Menu -->
			</div><!-- End Navigation Inner -->
		</nav>
		<!-- End Navigation -->